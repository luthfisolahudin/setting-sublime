Repository ini berisi konfigurasi [Sublime Text](https://www.sublimetext.com/) yang biasa digunakan saya.

---

Resource yang digunakan:
- Font [JetBrains Mono](https://www.jetbrains.com/lp/mono/)

---

Cara penggunaan:
- Untuk Windows:
  - Copy isi folder `conf` ke `%APPDATA%\Sublime Text\Packages\User`
